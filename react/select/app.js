import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

class FetchDoctors extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      doctors: [],
      selectedDoctor: null,
      specialties: []
    };
  }
  componentDidMount() {
    axios.get("http://localhost:3000/doctors")
      .then(res => {
        const doctors = res.data;
        this.setState({ doctors });
      });
  }
  getSpecialty(id) {
    let specialties = this.state.doctors[id - 1].specialest_name.specialties;
    this.setState({ specialties })
  }
  getSelected(event) {
    this.setState({ selectedDoctor: event.target.value })
    this.getSpecialty(event.target.value);
  }
  render() {
    return (
      <div>
        <select onChange={this.getSelected.bind(this)}>
          {this.state.doctors.map(doctor =>
            <option value={doctor.doctor_id} key={doctor.doctor_name}>{doctor.doctor_name}</option>
          )}
        </select>
        <select>
          {this.state.specialties.map(doc_spec =>
            <option value={doc_spec.id} key={doc_spec.id}>{doc_spec.name}</option>
          )}
        </select>
      </div>
    );
  }
}

ReactDOM.render(
  <FetchDoctors subreddit="reactjs" />,
  document.getElementById('app')
);